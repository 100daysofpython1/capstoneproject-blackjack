## The deck is unlimited in size. 
## There are no jokers. 
## The Jack/Queen/King all count as 10.
## The the Ace can count as 11 or 1.
## The cards in the list have equal probability of being drawn.
## Cards are not removed from the deck as they are drawn.
## The computer is the dealer.


# To Do List for Blackjack Game
# 1: Import the Random Module for the draws - DONE
# 2: Print out the Blackjack Logo - DONE
# 3: Give the user an input on the total amount of money - DONE
#  they want to have and save this into a variable

# 4: Make the Dealers Amount variable equal to the users amount - DONE
# 5: Give the user an input in how much they want to bet - DONE
# 5.1: Have checks so they can only enter a numeric value - DONE 
# 6: Deals the User two cards from the cards list (store it inside their own list) - DONE
# 6.1: If the user total amount adds up to 21 from the first two cards 
# they automatically stand and if the dealer doesn't have blackjack they win

# 7: User input for hit or stand - DONE
# 7.1: Give them another card if they say hit - DONE
# 7.1.1: If it is an ace check to see if it will go over 21. 
# If it goes over 21, ace = 1 otherwise ace = 11
# 7.1.2: Add the card to the player total variable
# 7.1.3: Check to see if the player total is over 21
# and if not goes back to hit/stand decison
# 7.1.4: If the player total goes over 21 
# but they currently have an ace that == 11, set that to 1

# 7.1.5: If they still go over 21 the player goes bust
#  and the amount they is doubled and goes to the dealers amount

# 7.2: If the player chooses stand the dealer hand is shown.
# 7.2.1: If the dealer hand is over 21 they go bust, 
# the player wins and the bet is doubled and added to the players amount
# 7.2.2: If the dealers hand is over the players hand, 
# the dealer wins, the bet is doubled and added to the dealers amount
# 7.2.3: If the dealer hand is equal to the players hand it is a draw 
# and the bet is added back the players total amount
# 7.2.4: If the dealers hand is under 17 they draw a card

# 8: Check to see if either the dealer or players total is over 0
# 8.1: If the players hand is not over 0 announce the dealer wins and end the program
# 8.1.1: If the dealers hand is not over 0 announce the player has won and end the program
# 8.2: Ask the player if they want to go for another round. 
# If they say yes, go back to 5 otherwise end the program

import random, art

cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
user_cards = []
dealer_cards = []
print(art.logo[0])
numeric_flag = False # Our Numeric Flag
while not numeric_flag: #Keeps checking along as it is False
    player_amount = input("Enter in the total amount of money that you have: £")
    numeric_flag = player_amount.isnumeric() # If a number has been entered it changes to true otherwise it equals False

    if numeric_flag: # Case for when the flag has been set to true
        player_amount = int(player_amount) # If it is a number it will then change the type

    else: # Gives the player guidance into what value they should enter
        print("Please enter a whole numeric value")

dealer_amount = player_amount

def bet():
    """
    This function creates a check to ensure the player's bet is numeric and to take it away from their total 
    amount of money they put in for the bet and deals the initial starting cards to the user
    """
    is_bet_numeric = False
    while not is_bet_numeric:
        player_bet_amount = input("Enter your total bet amount: £")
        is_bet_numeric = player_bet_amount.isnumeric()
        
        if is_bet_numeric:
            player_bet_amount = int(player_bet_amount)
            new_player_amount = player_amount - player_bet_amount
            return new_player_amount, player_bet_amount
        else:
            print("Enter a whole numeric value")
def card_dealing_user():
    """
    This function deals the inital starting cards to the user
    """
    for i in range(2):
        the_card = (random.randrange(0, len(cards) - 1)) # Gets a random card from the cards list which gets the index of the cards list
        card_deal = user_cards.append(cards[the_card])
    print(user_cards)
    return card_deal, user_cards

def draw_card():
    """
    This function is to be used when the player wants to draw another card that is not a part of the starting cards that they would have drawn.
    It will append the card to their current cards list.
    """
    next_card = (random.randrange(0, len(cards) - 1))
    draw_next_card = user_cards.append(cards[next_card])    

def HIT():
    """
    Will draw another card when the user says HIT
    """
    stand = False
    while not stand:
        another_card = input("Enter 'HIT' if yoou want to draw another card or 'STAND if you want to stay with the current cards.\n")
        if another_card == "HIT":
            draw_card()
            print(user_cards)
        elif another_card == "STAND":
            stand = True
        else:
            print("Please Enter either HIT or STAND\n")




bet()
card_dealing_user()
HIT()
